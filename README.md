# urgent-care-checker

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/urgent-care-checker.svg)](https://www.npmjs.com/package/urgent-care-checker) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save urgent-care-checker
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'urgent-care-checker'
import 'urgent-care-checker/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [cirodam](https://github.com/cirodam)
