import React from 'react'

import UrgentCareChecker from 'urgent-care-checker'
import 'urgent-care-checker/dist/index.css'

const App = () => {

  let urgents = [
    {
        name: "Gainesville",
        wait: 30,
        phone: "(770) 444-2222"
    },
    {
        name: "Dawsonville",
        wait: 17,
        phone: "(444) 111-7777"
    },
    {
        name: "Dacula",
        wait: 40,
        phone: "(444) 111-7777"
    },
]

  return <UrgentCareChecker urgents={urgents}/>
}

export default App
