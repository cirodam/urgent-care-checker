import React from 'react'
import styles from './styles.module.css';

const UrgentCareChecker = ({urgents}) => {

  return ( 
  
    <div className={styles.waitTimes}>
      <h2 className={styles.timesHeader}>Urgent Care Wait Times</h2>
      <ul className={styles.timesList}>
          {
              urgents.map(u => (
                  <li className={styles.timeItem}>
                      <h4 className={styles.timeHeader}>{u.name}</h4>
                      <div>
                          <p className={styles.timeDesc}><i className={`far fa-clock ${styles.leftMark}`}></i> {u.wait} minute wait</p>
                          <a href="https://google.com" className={styles.timeDirections}><i className={`fas fa-map-signs ${styles.leftMark}`}></i>Get Directions <i className={`fas fa-chevron-right ${styles.chev}`}></i></a>
                          <a href="https://google.com" className={styles.timeMap}><i className={`fas fa-map-marked-alt ${styles.leftMark}`}></i>View on map <i className={`fas fa-chevron-right ${styles.chev}`}></i> </a>
                          <p className={styles.timePhone}><i className={`fas fa-phone ${styles.leftMark}`}></i>{u.phone}</p>
                      </div>
                      <button className={styles.saveBtn}>Save My Spot!</button>
                  </li>
              ))
          }
      </ul>
    </div>

  )
}

export default UrgentCareChecker;